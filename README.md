# soal-shift-sisop-modul-1-ITA04-2022

## Laporan Pengerjaan Modul 1 Praktikum Sistem Operasi

Nama Anggota Kelompok

1. ANISAH FARAH FADHILAH
2. GILANG BAYU GUMANTARA
3. M. FERNANDO N. SIBARANI

---

## Soal 1

### Soal

Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

a. Han membuat sistem register pada script **register.sh** dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script **main.sh**

b. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : **MM/DD/YY hh:mm:ss MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.
1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah **REGISTER: ERROR User already exists**
2. Ketika percobaan register berhasil, maka message pada log adalah **REGISTER: INFO User USERNAME registered successfully**
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah **LOGIN: ERROR Failed login attempt on user USERNAME**
4. Ketika user berhasil login, maka message pada log adalah **LOGIN: INFO User USERNAME logged in**

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
1. `dl N` ( N = Jumlah gambar yang akan didownload).
    Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD_USERNAME**. Gambar-gambar yang didownload juga memiliki format nama **PIC_XX**, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
2. `att`.
    Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

### Cara Pengerjaan

#### **1a**
Dalam file **register.sh** dan **main.sh**, dibuat file **user.txt** di dalam direktori **users**
```bash
# Buat direktori users.txt
if ! [[ -d "users" ]]
then
    mkdir "users"
fi

user_file="users/user.txt"
#Check apakah file data user sudah ada
if ! [ -f "$user_file" ]
then
    touch "$user_file"
fi
```

#### **1b**
Untuk membuat input passwword hidden, digunakan `-sp` seperti berikut:
``` bash
read -p 'Username: ' username
read -sp 'Password: ' pass
```

Agar user akan menginput terus-menerus hingga password sesuai kriteria, kami menggunakan loop `while` dan variabel penentu berhasil dengan nilai awal true `berhasil=true`

Jika password yang diinputkan belum memenuuhi kriteria, variabel berhasil akan bernilai false `berhasil=false`
``` bash
if [ ${#pass} -lt 8 ]
then
    berhasil=false
    echo "Password minimal 8 karakter"
fi

if ! (echo "$pass" | grep -q "[A-Z]") && (echo "$pass" | grep -q "[a-z]") 
then
    berhasil=false
    echo "Password memiliki minimal 1 huruf kapital dan 1 huruf kecil"
fi

if ! (echo "$pass" | grep -q "[0-9 ]" )
then
    berhasil=false
    echo "Password harus alphanumeric"
fi

if [ "$username" == "$pass" ]
then
    berhasil=false
    echo "Password dan Username tidak boleh sama"
fi
```

#### **1c**

##### **log.txt**
Format penulisan log: `$(date '+%Y-%m-%d %H:%M:%S') MESSAGE`

Kami menggunakan `>> log.txt` untuk membuat file (jika belum ada) dan menyimpan teks ke dalam file (jika sudah terisi akan ditambahkan dengan teks baru (_append_))

##### **register.sh**
Jika password memenuhi kriteria (variabel berhasil tetap bernilai true), username dan password akan masuk ke fungsi `store()`
``` bash
if $berhasil
then
    store ${username,,} $pass
    break
else
    echo
fi
```

Dalam fungsi `store()` ini, akan dicek apakan username telah terdaftar atau belum. Jika sudah terdaftar, menampilkan pesan eror. Jika belum terdaftar, menampilkan pesan berhasil username dan password akan ditulis/di-_store_ ke dalam **user.txt**. Setiap pesan tersebut disimpan dalam **log.txt**
``` bash
store () {
    if grep -q "${1,,} " $user_file
    then
        echo  "$(date '+%Y-%m-%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
        echo "Username telah terdaftar"
    else
        echo  "$(date '+%Y-%m-%d %H:%M:%S') REGISTER: INFO User ${username,,} registered successfully" >> log.txt
        echo ${1,,} $2 >> $user_file
        echo "Registrasi Berhasil"
    fi
}
```

##### **main.sh**
Ketika user menjalankan file **main.sh**, akan diminta untuk login terlebih dahulu. Username dan password yang diinputkan akan dicek apakah sesuai dengan akun yang terdaftar dalam **user.txt**.

Jika berhasil,
``` bash
if grep -Fxq "${username,,} $password" $user_file
then
    echo  "$(date '+%Y-%m-%d %H:%M:%S')  LOGIN: INFO User ${username,,} logged in" >> log.txt
    echo "Login Berhasil"
```
Jika gagal,
``` bash
else
    echo  "$(date '+%Y-%m-%d %H:%M:%S')  LOGIN: ERROR Failed login attempt on user ${username,,}" >> log.txt
    echo "Username / Password Salah"
fi
```

#### **1d**

Setelah berhasil login, user dapat menginputkan perintah berkali-kali hingga program dihentikan
``` bash
while true
do
    read perintah
    $perintah
    echo
done
```

Pada perintah `dl`, akan dicek apakah zip dengan format YYYY-MM-DD_USERNAME sudah ada atau belum.

Jika sudah ada, file zip akan di-`unzip` kemudian file zip tersebut dihapus menggunakan `rm`
```bash
if [ -f "$(date '+%Y-%m-%d')_${username,,}.zip" ]
then
    echo "$(date '+%Y-%m-%d')_${username,,}.zip"
    unzip -P $password "$(date '+%Y-%m-%d')_${username,,}.zip"
    rm "$(date '+%Y-%m-%d')_${username,,}.zip"
fi
```

Setelah itu, dicek apakah direktori dengan format YYYY-MM-DD_USERNAME sudah ada atau belum. Jika belum ada, akan dibuat direktorinya. `file_number` adalah variabel untuk memberi nama file/gambar yang akan di-_download_.
```bash
if ! [[ -d "$(date '+%Y-%m-%d')_${username,,}" ]]
then
    file_number=0
    mkdir "$(date '+%Y-%m-%d')_${username,,}"
else
    file_number=1
fi
```

Kemudian, mencari/menghitung jumlah file gambar. Jika belum pernah men-_download_ gambar, `file_number` yang awalnya bernilai 0, akan menjadi 1. Jika sudah pernah, `file_number` yang awalnya bernilai 1, akan menjadi banyak file saat ini + 1. `file_number` ini akan digunakan sebagai penamaan file gambar yang akan di-_download_
```bash
for file in $(date '+%Y-%m-%d')_${username,,}/*.jpg
do
    file_number=$((file_number+1))
done
```

Kami menggunakan loop `for` untuk men-_download_ gambar sebanyak `N`
```bash
for ((num=1; num<=$1; num=num+1))
```

Untuk penamaan file jika kurang dari 10, akan diawali dengan angka 0.
```bash
if [[ $((10#$file_number)) -lt $((10#10)) ]]
then
    file_number="0${file_number}"
else
    file_number=${file_number}
fi
```

Kemudian _download_ gambar menggunakan `wget` dan disimpan dalam folder `$(date '+%Y-%m-%d')_${username,,}`
```bash
wget -O "$(date '+%Y-%m-%d')_${username,,}/PIC_$file_number.jpg" "https://loremflickr.com/320/240"
```

Terakhir, folder di-zip dan di-password sesuai dengan password user. Setelah di-zip, folder dihapus
```bash
zip --password $password "$(date '+%Y-%m-%d')_${username,,}.zip" $(date '+%Y-%m-%d')_${username,,}/*
rm -r $(date '+%Y-%m-%d')_${username,,}
```

Pada perintah `att`, kami menggunakan `awk` untuk menghitung banyak login yang berhasil dan gagal.

Login berhasil --> Menghitung banyak _line_ dalam file **log.txt** yang mengandung `/LOGIN: INFO User '"$username"' logged in/` dengan `$username` adalah username yang saat itu digunakan.
```bash
echo "Login baik : " `awk '/LOGIN: INFO User '"$username"' logged in/ { i++ } END{ print i}' log.txt`
```

Login gagal --> Menghitung banyak _line_ dalam file **log.txt** yang mengandung `/LOGIN: ERROR Failed login attempt on user '"$username"'/` dengan `$username` adalah username yang saat itu digunakan.
``` bash
echo "Login tidak baik : " `awk '/LOGIN: ERROR Failed login attempt on user '"$username"'/ { i++ } END{ print i }' log.txt`
```


### Screenshot

Screenshot register

![register](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/register.jpg)


Screenshot user.txt

![user](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/user.jpg)


Screenshot login

![login](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/login.jpg)


Screenshot log.txt

![log](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/log.jpg)


Screenshot command att

![att](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/att.jpg)


Screenshot command dl 2

![dl_2](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/dl_2.jpg)


Screenshot command dl 11 (penambahan)

![dl_11](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/tambah_dl_11.jpg)


Screenshot unzip hasil download

![unzip](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/unzip.jpg)


Screenshot folder gambar hasil download

![folder](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal1/folder_gambar.jpg)


### Kendala yang Dihadapi

1. Ketika download gambar lebih dari 8, eror, hanya ter-download 8 gambar. Sehingga perlu ditambahkan #10 di depan atau #0 di belakang


## Soal 3

### Soal

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah **/home/{user}/**.

a. Masukkan semua metrics ke dalam suatu file log bernama **metrics\_{YmdHms}.log**. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics*agg_2022013115.log dengan format metrics_agg*{YmdH}.log

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Note:
- nama file untuk script per menit adalah **minute_log.sh**
- nama file untuk script agregasi per jam adalah **aggregate_minutes_to_hourly_log.sh**
- semua file log terletak di **/home/{user}/log**
---
### Cara Pengerjaan

#### 3a

Dalam membuat file log, kami mengambil data waktu pada saat shell script dijalankan kemudian menggabungkannya dengan path file log yang akan dibuat. Dengan cara ini, variabel yang dibuat akan langsung mengarah ke file log yang akan diolah.

```bash
file_metrics="/home/ubuntu/log/metrics_$(date '+%Y%m%d%H%M%S').log"
```

Untuk mendapatkan data waktu mulai dari tahun sampai hari, kami menggunakan fungsi date yang diikuti dengan format waktu yang telah ditentukan oleh soal untuk penamaan file log.

Kemudian kami memasukkan string header untuk file log yang ditunjuk oleh variabel `file_metrics`

```bash
echo mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size > $file_metrics
```

Setelah membuat header, kemudian kami mengambil data dari perintah `free -m` dan mengolahnya dengan menggunakan awk.

Karena output dari perintah `free -m` terdapat 3 baris, yang dimana baris pertama berupa header, maka kami hanya mengambil data dari baris 2 dan baris 3

Setelah mendapatkan data dari `free -m`, kami juga mengambil data dari perintah `du -sh` yang diikuti dengan target path yang telah ditentukan oleh soal.

Setelah mendapatkan semua data, kami mendapati bahwa terdapat spasi yang memisahkan antara data monitoring ram antar baris dengan data monitoring disk. Karena hal ini, kami melakukan awk kembali untuk memperint 3 bagian tadi menjadi satu kesatuan yang hanya dipisahkan oleh koma saja.

Setelah mendapatkan semua data yang formatnya sudah disesuaikan, maka data metrics tersebut dimasukkan ke file log yang telah ditunjuk oleh `file_metics`

```bash
echo `free -m | awk '{if(NR==2) print $2","$3","$4","$5","$6","$7","} {if(NR==3) print $2","$3","$4",";}' && du -sh /home/ubuntu/ | awk '{print $2","$1}'` | awk '{ print $1 $2 $3}' >> $file_metrics
```

#### 3b

Untuk membuat script tersebut berjalan setiap menit, kami menggunakan cron.

Kami menggunakan perintah cron `* * * * *` untuk melakukan eksekusi setiap menit. Kemudian diikuti oleh command bash untuk mengeksekusi file bash, yang diikuti path file dari shell script yang telah kami buat.

```bash
* * * * * bash /home/ubuntu/praktikum_sisop/soal-shift-sisop-modul-1-ita04-2022/soal3/minute_log.sh
```

#### 3c

Sama seperti script log per menit, kami juga mengambil data waktu pada saat script dijalankan kemudian menggabungkannya dengan path file log perjam yang akan dikelola.

```bash
file_metrics="/home/ubuntu/log/metrics_agg_$(date '+%Y%m%d%H').log"
```

Untuk mendapatkan data dari file log per menit yang telah digenerate oleh script per menit yang telah dijalankan selama satu jam, maka kami data log yang telah diurutkan menurut waktu modifnya.

```bash
data=`ls -t /home/ubuntu/log/metrics_2*.log`
```

Dengan perintah di atas, akan didapat file log per menit yang telah diurutkan menurut waktu mulai dari yang terbaru.

Kemudian kami memasukkan string header ke file log yang telah ditunjuk oleh variabel `file_metrics`.

Kemudian setelah itu, kami mengelola semua data tiap file yang sudah diurutkan yang ada di variabel `data`.

```bash
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > $file_metrics
```

Untuk mendapatkan semua data dari file-file tersebut, kami menggunakan awk. Karena file log per menit yang digenerate terdapat header dan data, maka kami hanya akan mengambil data yang berada di baris genap.

```bash
awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data
```

Kemudian data-data tersebut kami olah lagi dengan menggunakan awk. untuk mendapatkan data minimum tiap metrics, kami mengiterasi semua baris kemudian menampung semua nilai tiap metrics di variabel-variabel yang berbeda. Jika ditemukan nilai yang lebih kecil dari variabel atau nilai variabel masih 0, maka nilai variabel akan diganti.

Ketika semua baris telah diiterasi, maka isi dari masing-masing variabel akan diprint dan dimasukkan ke file log agregasi.

```bash
echo 'minimum,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data | awk -F "," '{
                                                                    if (a>$1 || a==0){ a=$1 }
                                                                    if (b>$2 || b==0){ b=$2 }
                                                                    if (c>$3 || c==0){ c=$3 }
                                                                    if (d>$4 || d==0){ d=$4 }
                                                                    if (e>$5 || e==0){ e=$5 }
                                                                    if (f>$6 || f==0){ f=$6 }
                                                                    if (g>$7 || g==0){ g=$7 }
                                                                    if (h>$8 || h==0){ h=$8 }
                                                                    if (i>$9 || i==0){ i=$9 }
                                                                    if (k>$1 || k==0){k=$11}
                                                                    } END{print a","b","c","d","e","f","g","h","i","$10","k}'` >> $file_metrics
```

Hal yang sama juga kami lakukan untuk mencari data maksimum dari tiap metrics. perbedaannya nilai dari variabel akan diganti hanya jika nilai yang ditemukan itu lebih besar dari nilai yang sedang ditampung oleh variabel.

```bash
echo 'maksimum,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data | awk -F "," '{
                                                                    if (a<$1){ a=$1 }
                                                                    if (b<$2){ b=$2 }
                                                                    if (c<$3){ c=$3 }
                                                                    if (d<$4){ d=$4 }
                                                                    if (e<$5){ e=$5 }
                                                                    if (f<$6){ f=$6 }
                                                                    if (g<$7){ g=$7 }
                                                                    if (h<$8){ h=$8 }
                                                                    if (i<$9){ i=$9 }
                                                                    if (k<$11){k=$11}
                                                                    } END{print a","b","c","d","e","f","g","h","i","$10","k}'` >> $file_metrics
```

Kemudian untuk mencari rata-rata dari tiap data metrics, kami menampung semua nilai dari tiap metrics kemudian membaginya dengan jumlah iterasi yang dilakukan. Kemudian rata-rata yang telah didapatkan diprint kemudian diappend ke file log agregasi.

```bash
echo 'average,'`awk '(NR%2 == 0 && i<2*60) {print $1} {i++}' $data |  awk -F "," '{a+=$1;b+=$2;c+=$3;d+=$4;e+=$5;f+=$6;g+=$7;h+=$8;i+=$9;j=$10;k+=$11;n++} END{print a/n","b/n","c/n","d/n","e/n","f/n","g/n","h/n","i/n","j","k/n}'` >> $file_metrics
```

Untuk menjalan script agregasi tiap jam, maka digunakan cron dengan perintah `59 * * * *` yang diikuti dengan command bash dan diikuti oleh path script agregasi. Dengan demikian maka script agregasi akan dijalan setiap menit ke 59 hal ini akan mendapatkan rentang 1 jam dari menit 59 pertama dengan menit 59 kedua.

```bash
59 * * * * bash /home/ubuntu/praktikum_sisop/soal-shift-sisop-modul-1-ita04-2022/soal3/aggregate_minutes_to_hourly_log.sh
```

### Screenshot

Screenshot cronjob yang telah kami buat

![cronjob](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal3/cronjob.png)

Screenshot file log per menit yang telah digenerate

![file minute_log](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal3/file%20log%20minute.png)

Screenshot file log hasil script agregasi

![file agregasi](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal3/File%20log%20agregasi.png)

Screenshot folder log

![folder log](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-1-ita04-2022/Soal3/folder%20log.png)

### Kendala yang Dihadapi

1. Ketika memasukkan variabel yang berisi nama user ke path dan dirun melalui terminal, maka script berjalan normal dan menghasilkan file log yang seharusnya. Namun ketika memasukkan variabel `$USER` ataupun memasukkan hasil command `whoami` ke satu variabel untuk mendapatkan nama user dan memasukkannya ke path file, maka script tidak berjalan ketika menggunakan cron dan tidak menghasilkan file log di folder log. Namun pada saat menggunakan nama user yang ditulis langsung di path filenya, maka script dapat dijalankan dengan baik dengan cron. Namun hal ini membuat nama user yang ada pada script harus disesuaikan sebelum menjalankan file.